import {Component, OnInit} from '@angular/core';
import {Category} from "./category/category";
import {CategoryService} from "./category/category.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  categories: Category[];

  title = 'Hello world!';

  constructor(private categoryService: CategoryService) {}

  ngOnInit() {
    this.categoryService.getCategories().subscribe(data => this.categories = data);
  }

}
