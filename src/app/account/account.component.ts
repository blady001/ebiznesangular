import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {AccountService} from "./account.service";
import {Account} from "./account";

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  accountForm: FormGroup;
  account: Account;

  constructor(private accountService: AccountService) { }

  ngOnInit() {
    this.accountForm = new FormGroup({
      userId: new FormControl('userId', Validators.required),
      username: new FormControl('username', Validators.required),
      password: new FormControl('password', Validators.required),
      firstname: new FormControl('firstname', Validators.required),
      lastname: new FormControl('lastname', Validators.required),
      email: new FormControl('email', Validators.required),
      address: new FormControl('address', Validators.required)
    });

    // TODO: How to resolve user? - what auth?
    this.accountService.getAccount(1).subscribe(data => {
      this.account = data;
      this.accountForm.get('userId').setValue(this.account.userId);
      this.accountForm.get('username').setValue(this.account.username);
      this.accountForm.get('password').setValue(this.account.password);
      this.accountForm.get('firstname').setValue(this.account.firstname);
      this.accountForm.get('lastname').setValue(this.account.lastname);
      this.accountForm.get('email').setValue(this.account.email);
      this.accountForm.get('address').setValue(this.account.address);
    });
  }

  updateAccountData(event) {
    event.preventDefault();
    var data = this.accountForm.value;
    data["userId"] = this.account.userId;
    console.log(data);
    this.accountService.updateAccount(data, this.account.userId)
      .subscribe(
        data => {
          this.accountService.getAccount(this.account.userId).subscribe(data => this.account = data);
          console.log("SUCCESS!!");
        },
        error => console.log("SOMETHING WENT WRONG during updating!", error)
      );
  }

}
