/**
 * Created by macblady on 22.05.2017.
 */

export class Account {
  userId: number;
  username: string;
  password: string;
  firstname: string;
  lastname: string;
  email: string;
  address: string;
}
