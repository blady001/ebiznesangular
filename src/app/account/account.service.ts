/**
 * Created by macblady on 22.05.2017.
 */

import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";
import {Account} from "./account";

@Injectable()
export class AccountService {

  constructor(private http: Http) {}

  getAccount(userId: number) {
    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});

    return this.http.get('http://localhost:9000/user/' + userId, options).map(response => <Account>response.json());
  }

  updateAccount(formData, userId) {
    const serializedForm = JSON.stringify(formData);

    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});

    return this.http.post('http://localhost:9000/user/' + userId, serializedForm, options);
  }
}
