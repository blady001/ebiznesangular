/**
 * Created by macblady on 13.05.2017.
 */

export class Order {
  id: number;
  userId: number;
  totalAmount: string;
  isRealized: boolean;
}
