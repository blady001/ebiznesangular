/**
 * Created by macblady on 13.05.2017.
 */

import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";
import {Order} from "./order";
import 'rxjs/add/operator/map';

@Injectable()
export class OrderService {
  constructor(private http: Http) {

  }

  getOrders() {
    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});

    return this.http.get('http://localhost:9000/order', options).map(response => <Order[]>response.json());
  }

  getOrder(orderId: number) {
    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});

    return this.http.get('http://localhost:9000/order/' + orderId, options).map(response => <Order>response.json());
  }

  updateOrder(formData) {
    const serializedForm = JSON.stringify(formData);

    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});

    return this.http.post('http://localhost:9000/order/' + formData['id'], serializedForm, options);
  }

  getActiveOrderForUser(userId: number) {
    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});

    return this.http.get('http://localhost:9000/user/' + userId + '/activeorder', options).map(response => <Order>response.json());
  }
}
