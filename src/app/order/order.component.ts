import { Component, OnInit } from '@angular/core';
import {Order} from "./order";
import {OrderService} from "./order.service";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {Account} from "../account/account";
import {CartItem} from "../cartitem/cartitem";
import {AccountService} from "../account/account.service";
import {ProductService} from "../product/product.service";
import {CartItemService} from "../cartitem/cartitem.service";
declare var $: any;

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  order: Order;
  orderCheckoutForm: FormGroup;
  account: Account;
  cartItems: CartItem[];
  productsLoaded: number = 0;

  constructor(private orderService: OrderService, private accountService: AccountService,
              private productService: ProductService, private cartItemService: CartItemService) { }

  ngOnInit() {
    this.orderCheckoutForm = new FormGroup({
      // id: new FormControl('id', Validators.required),
      // userId: new FormControl('userId', Validators.required),
      // totalAmount: new FormControl('totalAmount', Validators.required),
      // isRealized: new FormControl('isRealized', Validators.required),
      paymentMethod: new FormControl('paymentMethod', Validators.required),
      shippingAddress: new FormControl('shippingAddress', Validators.required)
    });
    this.orderCheckoutForm.get('paymentMethod').setValue('paypal');

    // TODO: Again, how to fetch user
    this.accountService.getAccount(1).subscribe(data => {
      this.account = data;
      this.orderCheckoutForm.get('shippingAddress').setValue(this.account.address);
      // console.log("Fetched user: " + this.account);


      this.orderService.getActiveOrderForUser(this.account.userId).subscribe(
        data => {
          this.order = data;
        },
        error => console.log("Couldn't fetch order for user: ", error)

      );


      this.cartItemService.getCartItemsForUser(this.account.userId).subscribe(data => {
        this.cartItems = data;
        for (var item of this.cartItems) {
          console.log(item.productId);
          this.productService.getProductById(item.productId).subscribe(data => {
            this.cartItems.find(x => x.productId == data.id).product = data;
            this.productsLoaded += 1;
            console.log("Fetched product to cartItem: ", data);
          });
        }
      })
    },
    error => console.log("FIRST FETCH ERROR: ", error)
    );

  }

  isEverythingFetched() {
    return this.cartItems.length == this.productsLoaded;
  }

  getTotalAmount() {
    var total = 0.0;
    for (var item of this.cartItems) {
      total += Number(item.product.price) * item.quantity;
    }
    return total.toString();
  }

  realizeOrder(event) {
    event.preventDefault();
    var data = this.orderCheckoutForm.value;
    data['isRealized'] = true;
    data['totalAmount'] = this.getTotalAmount();
    data['id'] = this.order.id;
    data['userId'] = this.order.userId;
    console.log("Submitted data: ", data);

    this.orderService.updateOrder(data).subscribe(
      data => {
        $('#order-submission-form').replaceWith("<h2>Thank you! Your order is being processed.");
      },
      error => {
        console.log("Error", error);
        $('#order-submission-form').replaceWith("<h2>Unexpected error occured. Please try again later.");
      }
    )
  }
}
