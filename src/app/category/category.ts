/**
 * Created by macblady on 21.05.2017.
 */

export class Category {
  id: number;
  name: string;
  description: string;
}
