/**
 * Created by macblady on 21.05.2017.
 */

import { Injectable } from '@angular/core';
import {Headers, RequestOptions, Http} from "@angular/http";
import 'rxjs/add/operator/map';
import {Category} from "./category";

@Injectable()
export class CategoryService {
  constructor(private http: Http) {}

  getCategories() {
    const headers: Headers = new Headers();
    headers.append("Accept", "application/json");
    headers.append("Content-Type", "application/json");

    const options = new RequestOptions({headers: headers});
    return this.http.get('http://localhost:9000/category', options).map(response => <Category[]>response.json());
  }
}
