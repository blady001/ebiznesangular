/**
 * Created by macblady on 20.05.2017.
 */

import {Component, OnInit} from '@angular/core'
import {Product} from "../product/product";
import {ProductService} from "../product/product.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-category',
  templateUrl: './../product/product.component.html',
  styleUrls: ['./../product/product.component.css']
})
export class CategoryComponent implements OnInit {

  products: Product[];
  categoryId: String;

  constructor(private productService: ProductService, private route: ActivatedRoute){}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.categoryId = params['categoryId'];
    });
    this.productService.getProductByCategory(this.categoryId).subscribe(data => this.products = data);
  }
}
