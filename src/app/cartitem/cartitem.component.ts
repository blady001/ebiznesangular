import { Component, OnInit } from '@angular/core';
import {CartItem} from "./cartitem";
import {CartItemService} from "./cartitem.service";
import {ProductService} from "../product/product.service";
import {Router} from "@angular/router";
declare var $: any;

@Component({
  selector: 'app-cartitem',
  templateUrl: './cartitem.component.html',
  styleUrls: ['./cartitem.component.css']
})
export class CartitemComponent implements OnInit {

  cartItems: CartItem[];

  constructor(private cartItemService: CartItemService, private productService: ProductService, private router: Router) { }

  ngOnInit() {
    // TODO: How to fetch user ID? Session?
    this.cartItemService.getCartItemsForUser(1).subscribe(data => {
      this.cartItems = data;
      for (var item of this.cartItems){
        console.log(item.productId);
        this.productService.getProductById(item.productId).subscribe(data => {
          this.cartItems.find(x => x.productId == data.id).product = data;
        });
      }
    });
  }

  removeItemFromCart(event, cartItemId) {
    this.cartItemService.removeCartItem(cartItemId).subscribe(
      data => {
        $('#cart-element-'+cartItemId).remove();
        console.log("Removed!");
      },
      error => console.log("error!")
    );
  }

  updateCartItemInfo(event) {
    var cartItems = this.cartItems;
    $('.quantity-input').each(function(index, value) {
      var inputValue = parseInt($(value).val());
      // console.log("Input value: " + inputValue);
      var inputName = $(value).attr('name');
      // console.log("inputName: " + inputName);
      var splitted = inputName.split('-');
      var itemId = parseInt(splitted[splitted.length - 1]);
      // console.log('Cart item ID: ' + itemId);

      cartItems.find(x => x.id == itemId).quantity = inputValue;

    });
    for (var cartItem of this.cartItems) {
      this.cartItemService.updateCartItem(cartItem).subscribe(
        data => {
          console.log("Succesfully updated: " + cartItem.id);
        },
        error => console.log("error: ", error)
      )
    }
    this.router.navigate(['order']);
  }

}
