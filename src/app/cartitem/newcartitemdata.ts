/**
 * Created by macblady on 21.05.2017.
 */

export class NewCartItemData {
  constructor(
    public userId: number,
    public username: string,
    public password: string,
    public productId: number
  ) {}
}
