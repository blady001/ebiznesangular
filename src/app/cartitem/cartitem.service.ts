/**
 * Created by macblady on 13.05.2017.
 */

import 'rxjs/add/operator/map';
import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";
import {CartItem} from "./cartitem";
import {NewCartItemData} from "./newcartitemdata";

@Injectable()
export class CartItemService {

  constructor(private http: Http) {

  }

  getCartItemsForUser(userId: number) {
    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});

    return this.http.get('http://localhost:9000/user/' + userId + '/cart', options).map(response => <CartItem[]>response.json());
  }

  addToCart(userId: number, username: string, password: string, productId: number) {
    const payloadObj = new NewCartItemData(userId, username, password, productId);
    const payload = JSON.stringify(payloadObj);

    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({headers: headers});

    return this.http.post('http://localhost:9000/cartitem/add', payload, options);
  }

  removeCartItem(cartItemId: number) {
    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});
    return this.http.post('http://localhost:9000/cartitem/' + cartItemId + "/delete", options);
  }

  updateCartItem(cartItem: CartItem) {
    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    const options = new RequestOptions({headers: headers});

    const payloadObj = {
      "id": cartItem.id,
      "productId": cartItem.productId,
      "orderId": cartItem.orderId,
      "quantity": cartItem.quantity
    };

    const payload = JSON.stringify(payloadObj);
    console.log("Update payload: "+ payload);

    return this.http.post('http://localhost:9000/cartitem/'+cartItem.id, payload, options);

  }

}
