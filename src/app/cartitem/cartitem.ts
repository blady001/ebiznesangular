import {Product} from "../product/product";
/**
 * Created by macblady on 13.05.2017.
 */

export class CartItem {
  id: number;
  productId: number;
  orderId: number;
  quantity: number;
  product: Product;
}
