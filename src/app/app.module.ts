import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import {RouterModule} from "@angular/router";
import {ProductService} from "./product/product.service";
import { OrderComponent } from './order/order.component';
import {OrderService} from "./order/order.service";
import { CartitemComponent } from './cartitem/cartitem.component';
import {CartItemService} from "./cartitem/cartitem.service";
import {CategoryComponent} from "./category/category.component";
import {CategoryService} from "./category/category.service";
import { AccountComponent } from './account/account.component';
import {AccountService} from "./account/account.service";

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    OrderComponent,
    CartitemComponent,
    CategoryComponent,
    AccountComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot([
      {path: '.', component: AppComponent},
      {path: 'product', component: ProductComponent},
      {path: 'order', component: OrderComponent},
      {path: 'cart', component: CartitemComponent},
      {path: 'account', component: AccountComponent},
      // {path: 'category/:categoryId', component: CategoryComponent}
    ])
  ],
  providers: [ProductService, OrderService, CartItemService, CategoryService, AccountService],
  bootstrap: [AppComponent]
})
export class AppModule { }
