import {Component, OnInit, OnDestroy} from '@angular/core';
import {ProductService} from "./product.service";
import {Product} from "./product";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {ActivatedRoute, Params} from "@angular/router";
import {CartItemService} from "../cartitem/cartitem.service";
import {Subscription} from "rxjs";
declare var $: any;

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit, OnDestroy {

  products: Product[];

  productForm: FormGroup;

  private subscription: Subscription;

  constructor(private productService: ProductService, private route: ActivatedRoute, private cartItemService: CartItemService) { }

  ngOnInit() {
    var categoryId: string = null;
    this.subscription = this.route.queryParams.subscribe((params: any) => {
      // console.log("PARAMS: " + params['categoryId']);
      categoryId = params['categoryId'];
    });

    if (categoryId != null)
      this.productService.getProductByCategory(categoryId).subscribe(data => this.products = data);
    else
      this.productService.getProducts().subscribe(data => this.products = data);

    this.productForm = new FormGroup({
      id: new FormControl('id'),
      name: new FormControl('name', Validators.required),
      description: new FormControl('description', Validators.required),
      price: new FormControl('price', Validators.required),
      amount: new FormControl('amount', Validators.required),
      category: new FormControl('category', Validators.required)
    });
  }

  createProduct(event) {
    console.log("Creating...");
    this.productService.createProduct(this.productForm.value);
  }

  addToCart(event, product) {
    // TODO: again user stuff
    this.cartItemService.addToCart(1, "admin", "admin", product.id)
      .subscribe(
        data => {
          var clickedButton = $('#add-to-cart-'+product.id);
          clickedButton.attr('disabled', true);
          clickedButton.html('Added');
          console.log("added!");
        },
        error => console.log("ERROR WHILE ADDING TO CART!", error)
      );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
