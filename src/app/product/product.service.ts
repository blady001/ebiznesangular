import { Injectable } from '@angular/core';
import {Headers, RequestOptions, Http} from "@angular/http";
import {Product} from "./product";
import 'rxjs/add/operator/map';

@Injectable()
export class ProductService {

  constructor(private http: Http) {

  }

  getProducts() {
    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});

    return this.http.get('http://localhost:9000/product', options).map(response => <Product[]>response.json());
  }

  getProductByCategory(categoryId: String) {
    // TODO: Implement this method
    // console.log("Called with: " + categoryId);
    return this.http.get('http://localhost:9000/category/' + categoryId).map(response => <Product[]>response.json());
  }

  getProductById(productId: number) {
    return this.http.get('http://localhost:9000/product/' + productId).map(response => <Product>response.json());
  }

  createProduct(formData) {
    const serializedForm = JSON.stringify(formData);

    const headers: Headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({headers: headers});

    this.http.post('http://localhost:9000/product/create', serializedForm, options)
      .subscribe(
        data => console.log("sent!", data),
        error => console.log("doesnt work!", error)
      );

  }

}
