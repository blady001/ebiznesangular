/**
 * Created by macblady on 08.05.2017.
 */

export class Product {
  id: number;
  name: string;
  description: string;
  price: string;
  amount: number;
  category: string;
  imageUrl: string;
}
